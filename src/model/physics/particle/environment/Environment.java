package model.physics.particle.environment;

/**
*
* Represents the environment where the physical phenomenon takes place.
*
* @author Nicola Zamagni
*
*/
public interface Environment {

}
