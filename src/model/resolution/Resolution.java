package model.resolution;

/**
 *
 * @author Andrea Manoni
 *
 */
public interface Resolution {
    /**
     *
     * @return name.
     */
    String getName();

    /**
     *
     * @return x.
     */
    double getX();

    /**
     *
     * @return y.
     */
    double getY();
}
