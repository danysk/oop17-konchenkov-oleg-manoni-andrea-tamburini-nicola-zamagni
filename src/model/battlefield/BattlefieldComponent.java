package model.battlefield;



/**
 * Represent a component of the Environment.
 *
 * @author Andrea Manoni
 *
 */
public interface BattlefieldComponent {

    /**
     * This component name.
     *
     * @return name
     */
    String getName();
}
