package controller.matchmanager;
/**
 * 
 * @author Oleg
 *
 */
public enum GameModeType {
    /**
     * game whit only one player that have infinite projectiles, in order to improve his skills in the game! 
     */
    DEMO,
    /**
     * classic turn based game up to ten players!
     */
    MATCH,
}
